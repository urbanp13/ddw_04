# -*- coding: utf-8 -*-

import nltk
import numpy as np
from networkx.drawing.layout import _rescale_layout, _fruchterman_reingold, _sparse_fruchterman_reingold
from networkx.drawing.nx_agraph import graphviz_layout
from nltk.corpus import brown
from collections import Counter
from string import punctuation
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from nltk.stem import WordNetLemmatizer
from nltk.sentiment.util import *
from nltk.sentiment import SentimentIntensityAnalyzer
import networkx as nx
from os import path
from wordcloud import WordCloud
import matplotlib.pyplot as plt
from itertools import combinations
from pylab import show
import unicodedata


encoding = "utf-8"

def deg_cen(G):
    centrality={}
    s=1.0/(len(G)-1.0)
    centrality=dict((n,d*s) for n,d in G.degree_iter())
    return centrality

def keyNodes(G, centr):
    ranking = centr(G).items()
    ranking.sort(key=lambda x: x[1], reverse=True)
    #print ranking
    '''
    r = [x[1] for x in ranking]
    m = sum(r)/len(r) # mean centrality
    t = m*3 # threshold
    cg = G.copy()
    for k, v in ranking:
        if v < t:
            cg.remove_node(k)

    print cg.number_of_nodes()
    spring_pos = nx.spring_layout(cg)
    plt.axis("off")
    nx.draw_networkx(cg, pos=spring_pos, labels={v: str(v) for v in cg}, cmap=plt.get_cmap("bwr"))
    plt.savefig(centr.__name__ + ".png")
    plt.show()'''
    return ranking[:3]


def remove_accents(input_str):
    nfkd_form = unicodedata.normalize('NFKD', input_str)
    return u"".join([c for c in nfkd_form if not unicodedata.combining(c)])

def tokenCounts(tokens):
    counts = Counter(tokens)
    sortedCounts = sorted(counts.items(), key=lambda count:count[1], reverse=True)
    return sortedCounts

def extractEntities(ne_chunked):
    data = {}
    for entity in ne_chunked:
        if isinstance(entity, nltk.tree.Tree):
            text = " ".join([word for word, tag in entity.leaves()])
            ent = entity.label()
            data[text] = ent
        else:
            continue
    return data


text = None
with open('casts.csv', 'r') as f:
    text = f.read()
lines = text.splitlines()

actors = []
cast = []
movieCasts = []
l = lines[0].decode(encoding)
l = remove_accents(l)
cells = l.replace('"', '').split(';')
prevMovie = movie = cells[1]
actor = cells[2]
cast.append(actor)
actors.append(actor)
f = 0

for line in lines:
    #f=f+1
    line = line.decode(encoding)
    line = remove_accents(line)
    cells = line.replace('"', '').split(';')
    movie = cells[1]
    actor = cells[2]
    if actor == "s a" or actor == "":      #clearly not actor name, skip
        continue
    actors.append(actor)
    if movie == prevMovie:
        cast.append(actor)
    else:
        #if f > 100:
         #   break
        movieCasts.append(cast)
        cast = []
        cast.append(actor)
        prevMovie = movie
        #f = f + 1


movieCasts.append(cast)        #add last movie cast
actors = list(set(actors))

#for c in movieCasts:
   # print c

G = nx.Graph()

for actor in actors:
    #print actor
    G.add_node(actor)

for cast in movieCasts:
    edges = combinations(cast, 2)
    G.add_edges_from(edges)
    
numOfNodes = G.number_of_nodes()
numOfEdges = G.number_of_edges()
avgDegree = 2.0*numOfEdges/numOfNodes
density = (2*(float)(numOfEdges))/(numOfNodes*(numOfNodes-1))
numOfComponents = nx.number_connected_components(G)

# write to GEXF
nx.write_gexf(G, "export.gexf")

print "Number of nodes is", numOfNodes
#print G.nodes()
print "Number of edges is", numOfEdges
#print G.edges()
print "Average degree is", avgDegree
print "Density of the graph is ", density
print "Number of components of the graph is ", numOfComponents, "\n\n"

keyActors = set()


ranking = nx.degree_centrality(G).items()
ranking.sort(key=lambda x: x[1], reverse=True)
r = [x[1] for x in ranking]
m = sum(r) / len(r)  # mean centrality
t = m * 15  # threshold
cg = G.copy()
for k, v in ranking:
    if v < t:
        cg.remove_node(k)


numAct = cg.nodes().__len__()
#print numAct
#print cg.nodes()
'''
spring_pos = nx.spring_layout(cg)
plt.axis("off")
ccg = nx.degree_centrality(cg)
nx.draw(cg, spring_pos, labels={v: str(v) for v in cg},
        cmap=plt.get_cmap("bwr"), node_color=[ccg[k] for k in ccg])
plt.savefig("degree_centrality.png")
plt.show()
'''

for actor in ranking[:numAct]:
    keyActors.add(actor[0])

#print "Number of selected key players is", keyActors.__len__()
#print keyActors

for actor in keyActors:
    paths = nx.single_source_shortest_path(G, actor)
    lengths = nx.single_source_shortest_path_length(G, actor)
    sumOfBaconNumbers = (float)(sum(lengths.values()))
    paths = sorted(paths.items(), key=lambda x: len(x[1]), reverse=True)
    lengths = sorted(lengths.items(), key=lambda x: x[1], reverse=True)
    numOfConActors = len(lengths)
    #print lengths[:5]
    #print paths[:5]
    print "Kevin Bacon numbers for", actor
    print "Average Kevin Bacon number for", actor, "is", sumOfBaconNumbers/numOfConActors
    print "Max Kevin Bacon number for", actor, "has", lengths[0][0], "-", lengths[0][1]
    print "The path goes like this -", paths[0][1], "\n"



communities = {node:cid+1 for cid,community in enumerate(nx.k_clique_communities(G,3)) for node in community}
communities = sorted(communities.items(), key=lambda x: x[1], reverse=True)
#print communities.__len__()
#print communities



com = []
actorComs = []
num = prevNum = communities[0][1]
com.append(communities[0][0])
for actor in communities[1:]:
    name = actor[0]
    num = actor[1]
    if num == prevNum:
        com.append(name)
    else:
        actorComs.append(com)
        com = []
        com.append(name)
        prevNum = num

actorComs.append(com)  # add last community
actorComs.sort(key=len, reverse=False)

#for comunity in actorComs:
    #print comunity
newComs = []
c = 1
to_keep = []
for comunity in actorComs:
    if len(comunity) >= 15 and len(comunity) <= 20:
        for name in comunity:
            tmp = list()
            tmp.append(name)
            tmp.append(c)
            newComs.append((name, c))
            to_keep.append(name)
        c = c + 1

comGraph = G.subgraph(to_keep)

#print newComs
dictCom = dict(newComs)
#print dictCom
communities = {node:cid+1 for cid,community in enumerate(nx.k_clique_communities(G,3)) for node in community}


spring_pos = nx.spring_layout(comGraph)
plt.axis("off")
'''labels={v:str(v) for v in comGraph,'''
nx.draw_networkx(comGraph, with_labels=False,
        cmap = plt.get_cmap("rainbow"),
        node_color=[dictCom.get(node, 0.25) for node in comGraph.nodes()],
        node_size=25, width=0.5)
plt.savefig("coms.png")
plt.show()



